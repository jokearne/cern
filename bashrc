# .bashrc
if [ -f $HOME/.env ]; then
    . $HOME/.env
fi

if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi


export M2_HOME=/local/apache-maven-3.0.5
export MAVEN_HOME=$M2_HOME
export M2=$M2_HOME/bin
export PATH=$M2:$PATH
export USER=/user/jokearne
export FESA_CLASS_ROOT=/user/jokearne/dev/fesa-classes

alias make-fesa='make --jobs LEGACY_GCC=false LEGACY_WARNINGS=false'
alias V="source /user/jokearne/dev/python/venv/bin/activate"
alias H="cd /user/jokearne/"
alias F="cd /user/jokearne/dev/fesa-classes"

cd_data () {
    if [[ ! $(ls -1 /etc/fec_* | wc -l) -eq "0" ]]; then
        cd /dsc/local/data
    fi
}

get-run-command () {

      if [[ $1 == "-h" ]]; then
        echo "${FUNCNAME[0]}:"
        echo "      A runner script with some common use cases. At the moment, always run from /dsc/local/data"
        echo "      ${FUNCNAME[0]} [class-name]         -- Run the \"class-name\" DU from $FESA_CLASS_ROOT, with a local instance file"
        echo "      ${FUNCNAME[0]} [instance file]      -- Run the \"class-name\" DU from $FESA_CLASS_ROOT, with the specified instance file"
        return 0
      fi

      if [[ $# -ne 0 ]]; then
        if [[ -f $1 ]]; then
            INSTANCE=$(realpath $1)
        else
            shopt -s nocaseglob
            MATCHED=$(ls -1d $FESA_CLASS_ROOT/$1*)
            COUNT=$(printf "%s\n" $MATCHED | wc -l)
            shopt -u nocaseglob
            if [[ $COUNT -eq 2 ]]; then
                pushd $(printf "%s\n" $MATCHED | grep -v DU) > /dev/null
            else
                return 1
            fi
        fi
      fi

      ROOT=$(basename $(realpath $(pwd)))
      CLASS="${ROOT%_DU}"

      if [[ -f src/"$CLASS".design ]]; then
        IS_CLASS=true
        BIN_PATH=`pwd`/run/DU/build/bin/L867/"$CLASS"_DU_M
        DU_PATH=`pwd`/run/DU
      elif [[ -f src/"$CLASS"_DU.deploy ]]; then
        IS_DU=true
        BIN_PATH=`pwd`/build/bin/L867/"$CLASS"_DU_M
        DU_PATH=`pwd`
      elif [[ $(pwd) == /dsc/local/data ]] && [[ ! -z $INSTANCE ]]; then
        DU=$(echo $(basename $INSTANCE) | cut -f1 -d".")
        CLASS="${DU%_DU}"
        BIN_PATH=
        return 1
      else
        echo "${FUNCNAME[0]}: neither a class nor a deploy unit"
        return 1
      fi

      if [[ ! -f "$BIN_PATH" ]]; then
        BIN_NAME=$(basename $BIN_PATH)
        echo "${FUNCNAME[0]}: ${BIN_NAME} does not exist. run fesa-make first to build."
      fi

      FEC=$(hostname | cut -f1 -d".")

      if [[ -z $INSTANCE ]]; then
        INSTANCE=("$DU_PATH/src/test/$FEC/*$CLASS*.instance")
      fi

      echo "$BIN_PATH -instance $INSTANCE"

      popd > /dev/null
}

run-class () {

    if [[ ! -z $(hostname | grep "^cwe-") ]]; then
        fecs=$(find /acc/src/dsc/{sps,cps} -maxdepth 1 -mindepth 1 -iname "$1*")
        if [[ $(printf "%s\n" $fecs | wc -l) -ne 1 ]]; then
            echo "${FUNCNAME[0]}: ${1}* matches more than 1 FEC. Refine it."
            return 1
        fi
        ssh "$(basename $fecs)" "pushd /dsc/local/data; eval get-run-command "${@:2}""
    else
        pushd /dsc/local/data > /dev/null
        eval $(get-run-command "${@}")
        popd > /dev/null
    fi
}

cd /user/jokearne
